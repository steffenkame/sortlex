﻿using System;
using System.IO;
using System.Collections.Generic;
using Sortlex3;
using NLog;

namespace Sortlex
{
    class MainClass
    {        
        public static void Main(string[] args)
        {
            var logger = LogManager.GetCurrentClassLogger();

            var files = new List<string>()
            {
                @"C:\Users\User\Dropbox\lex-home.txt",
                @"C:\Users\User\Dropbox\lex-work.txt",
                @"C:\Users\User\Desktop\diary.txt"
            };

            foreach (string file in files)
            {
                if (!File.Exists(file))
                {
                    logger.Error($"File '{file}' doesn't exist.");
                }
            }

            try
            {
                foreach (string myfile in files)
                {
                    logger.Info($"Processing file: {myfile}");

                    try
                    {
                        var allText = File.ReadAllText(myfile);
                        logger.Info("LexCleanUp started");
                        LexCleanUp.Run(myfile, allText);

                        var allLines = File.ReadAllLines(myfile);
                        logger.Info("SortLines started");
                        SortLex.SortLines(myfile, allLines);
                    }
                    catch (Exception ex)
                    {
                        logger.Error(ex);
                        throw new Exception("LexCleanUp or SortLines doesn't work");
                    }

                    try
                    {
                        logger.Info("TagStats started");
                        Stats.TagStats(myfile);
                    }
                    catch (Exception ex)
                    {
                        logger.Error(ex);
                        throw new Exception("TagStats() doesn't work");
                    }
                    
                    logger.Info("DuplicateEntryCheck started");
                    DuplicateEntryCheck.Run(myfile);
                }

                // log size of all files
                int sizeSum = 0;
                foreach (string myfile in files)
                {
                    sizeSum += (int)new System.IO.FileInfo(myfile).Length;
                }
                sizeSum = sizeSum / 1000;
                File.AppendAllText(@"C:\Users\User\Dropbox\data\lex_articles.txt", $"{DateTime.Now} - {sizeSum}"
                    + Environment.NewLine);
            }
            catch(Exception ex)
            {
                logger.Error(ex);
                throw new Exception(ex.ToString());
            }
            
            Console.ReadKey();
        }        
    }
}

﻿using NLog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;

namespace Sortlex3
{
    class LexCleanUp
    {
        public static void Run(string myfile, string inputLineArray)
        {
            var logger = LogManager.GetCurrentClassLogger();

            // show bad brackets
            Regex regex = new Regex(@"\[\w*\}");
            foreach (var line in inputLineArray.Split('\n'))
            {
                Match match = regex.Match(line);
                if (match.Success)
                {
                    Console.WriteLine(match.Value);
                    logger.Error($"Bad bracket --> {match.Value}");
                }
            }


            // replace wrong tags
            var tagTranslator = new Dictionary<string, string>
            {
                //{ @"\[CSharp\]\[CSharp\]", "[CSharp]" },
                { @"\n\r\n\r\n", "\n\r\n" },  // remove duplicate line
                { @"#\r\n#\r\n#", "#\r\n#" },
                { @"!!!!", "!!!" },
                { @"\?\?\?\?", "???" },
                { @"\[IT\]\[Programmieren\]", "[Programmieren]" },
                { @"\[osi-pi\]", "[OSI-PI]" },
                { @"\[IT\]\[Hardware\]", "[Hardware]" },
                { @"\[EntityFramework\]", "[Entity-Framework]" },
                { @"\[Umweltschutz\]", "[Umwelt]" },
                { @"\[vbnet\]", "[VB.NET]" },
                { @"\[Collection\]", "[Collections]" },
                { @"\[CSharp\]\[List\]", "[CSharp][Collections][List]" },
                { @"\[CSharp\]\[String\]", "[CSharp][Type][String]" },
                { @"\[CSharp\]\[Array\]", "[CSharp][Type][Array]" },
                { @"^\[Entity-Framework\]", "[CSharp][Entity-Framework]" },
                { @"\[CSharp\]\[Design\-pattern\]", "[Programmieren][Design-pattern]" },
                { @"\[Tiere\]", "[Tier]" },
                { @"\[CSharp\]\[Time\]", "[CSharp][Timing]" },
                { @"\n\[WPF\]", "\n[CSharp][WPF]" },
                { @"\[Literatur\]", "[Buch]" },
                { @"Console\.WriteLine\s\(", "Console.WriteLine(" },
                { @"Console\.Write\s\(", "Console.Write(" },
                { @"sw\.WriteLine\s\(", "sw.WriteLine(" },
                { @"sw\.Write\s\(", "sw.Write(" },
                { @"\[istqb\]", "[ISTQB]" },
                { @"\.Replace\s\(", ".Replace(" },
                { @"\[Design-Pattern\]", "[Design-pattern]" }
            };

            foreach (KeyValuePair<string, string> entry in tagTranslator)
            {
                string inputLineArrayTemp = Regex.Replace(inputLineArray, entry.Key, entry.Value);
                inputLineArray = inputLineArrayTemp;
            }

            File.WriteAllText(myfile, inputLineArray);

            // looking for wrong sorted tags
            if (myfile.Contains("lex-home"))
            {
                if (inputLineArray.Contains("[privat]"))
                {
                    logger.Warn("[privat] in lex-home!");
                }
                if (inputLineArray.StartsWith("[Testing]"))
                {
                    logger.Warn("[Testing] in lex-home!");
                }
            }

            if (myfile.Contains("lex-work"))
            {
                if (inputLineArray.Contains("[privat]"))
                {
                    logger.Warn("[privat] in lex-work!");
                }
                if (inputLineArray.Contains("[Python]"))
                {
                    logger.Warn("[Python] in lex-work!");
                }
                if (inputLineArray.Contains("[Buch]"))
                {
                    logger.Warn("[Buch] in lex-work!");
                }
            }

            if (myfile.Contains("diary"))
            {
                // nothing
            }
        }
    }
}

- this tool is for managing my lex-files / note-files which are available in different txt-files
- all files have the following scheme

##myFirstArticle\n
[baseCategory][subCategory]\n
some text\n
more text\n

##mySecondArticle\n
[baseCategory][subCategory][subCategory][subCategory]\n
some text\n
more text\n

- the tool is sorting all articles in the note-file depedent on the baseCategory, then on the subCategory
- it will make a statistic about the tags (the categories)
- it will tell you if the article is in the wrong text-file
- it will look for duplicates
- it will correct the tags if wrong tag names were used
- duplicate empty lines will be corrected to one empty line